from django.urls import path
from projects.views import list_project, list_tasks, create_project


urlpatterns = [
    path("", list_project, name="list_projects"),
    path("<int:id>/", list_tasks, name="show_project"),
    path("create/", create_project, name="create_project"),
]
