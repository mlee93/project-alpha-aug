from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_project(request):
    project = Project.objects.filter(owner=request.user)
    context = {"project_object": project}
    return render(request, "projects/list_project.html", context)


def list_tasks(request, id):
    task = get_object_or_404(Project, id=id)
    context = {
        "task_object": task,
    }
    return render(request, "projects/list_tasks.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
        context = {
            "create_project_form": form,
        }
        return render(request, "projects/create_project.html", context)
